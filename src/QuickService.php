<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-26 16:45:18
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 22:05:31
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : QuickService.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\Reader;
use think\admin\command\ActionCommand;
use think\admin\command\InstallCommand;
use think\admin\command\ModelCommand;
use think\admin\command\PluginCommand;
use think\admin\command\QueueCommand;
use think\admin\command\ResourceCommand;
use think\admin\library\service\PluginService;
use think\admin\multiple\MultiApp;
use think\admin\multiple\Url;
use think\App;
use think\Cache;
use think\Config;
use think\facade\Lang;
use think\Service;

/**
 * 模块注册服务
 * Class QuickService
 * @package think\admin
 */
class QuickService extends Service
{
    /**
     * 启动服务
     */
    public function boot()
    {
        $this->app->event->listen('HttpRun', function () {
            $this->app->middleware->add(MultiApp::class);
            $this->app->middleware->add(function ($request, \Closure $next) {
                // 获取模块启动文件
                $path = app_path() . "QuickService.php";
                if (!is_file($path)) {
                    $path = app_path() . "Plugin.php";
                }

                if (is_file($path)) {
                    $resource = str_replace(
                        ['/', '.php'],
                        ['\\', ''],
                        strAfter($path, root_path())
                    );
                    try {
                        /** @var QuickPluginService $service */
                        invoke($resource)->boot();
                    } catch (\Exception $e) {

                    }
                }
                return $next($request);
            });
        });

        $this->registerCommand();

        $this->app->bind([
            'think\route\Url' => Url::class,
        ]);

        $this->loadPlugin();
        if (is_file(__DIR__ . '/helper.php')) {
            include_once __DIR__ . '/helper.php';
        }
    }

    public function loadPlugin()
    {
        if (!file_exists(app()->getRootPath() . '/install.lock')) {
            return  false;
        }
        PluginService::instance()->bootPlugins();
    }

    public function register()
    {
        AnnotationReader::addGlobalIgnoredName('mixin');
        // TODO: this method is deprecated and will be removed in doctrine/annotations 2.0
        $this->app->bind(Reader::class, function (App $app, Config $config, Cache $cache) {
            $store = $config->get('annotation.store');
            return new CachedReader(new AnnotationReader(), $cache->store($store), $app->isDebug());
        });
        $this->app->bind('JsonResponse', 'think\\admin\\http\\response\\JsonResponse');
        Lang::load(__DIR__ . '/lang/zh-cn.php', 'zh-cn');
        Lang::load(__DIR__ . '/lang/en-us.php', 'en-us');
        $this->loadRoutesFrom(__DIR__ . '/http/route/route.php');
        $this->registerJsonVariables();
    }

    /**
     * 注册quick json variables
     */
    protected function registerJsonVariables()
    {
        Quick::registerAssets(function () {
            //注册默认多语言配置
            $lang = config('quick.default_lang') ? config('quick.default_lang') : "zh-cn";
            Quick::translations(
                root_path('app/common/lang/quick') . $lang . ".json"
            );
            Quick::provideToScript([
                'timezone' => 'timezone',
                'lang' => config('quick.default_lang'),
                'translations' => Quick::allTranslations(),
                'locale' => 'locale',
            ]);
        });
    }

    /**
     *
     */
    protected function registerCommand()
    {
        $this->commands([
            ActionCommand::class,
            InstallCommand::class,
            ModelCommand::class,
            QueueCommand::class,
            ResourceCommand::class,
            PluginCommand::class,
        ]);
    }
}