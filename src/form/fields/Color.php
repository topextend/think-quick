<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 17:37:40
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:05:37
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Color.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\form\fields;

use think\admin\Element;
use think\Exception;

class Color extends Field
{
    public $component = 'form-color-field';

    /**
     * @var
     */
    public $default;

    /**
     * 辅助文字的颜色
     * @param string $color
     * @return $this
     */
    public function textColor(string $color)
    {
        $this->props("text-color", $color);
        return $this;
    }

    /**
     * 预定义颜色
     * @param array $colors
     * @return $this
     */
    public function predefine(array $colors)
    {
        $this->props("predefine", $colors);
        return $this;
    }

    /**
     * 支持透明度选择
     * @return $this
     */
    public function alpha()
    {
        $this->props("show-alpha", true);
        return $this;
    }

    /**
     * 写入 v-model 的颜色的格式
     * @param $format hsl|hsv|hex|rgb
     * @return $this
     */
    public function format($format)
    {
        $this->props("color-format", $format);
        return $this;
    }

    /**
     * Prepare the field for JSON serialization.
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), []);
    }
}