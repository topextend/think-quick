<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 14:15:04
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:04:57
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : SelectArea.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\form\fields;

use app\common\model\SystemArea;
use Closure;
use think\admin\Element;
use think\admin\metable\Metable;
use think\Exception;

class SelectArea extends Field
{
    public $component = 'form-select-area-field';

    /**
     * @var string
     */
    protected $valueType = 'array';

    /**
     * @var
     */
    protected $max;

    /**
     * @var
     */
    protected $min;

    /**
     * @var
     */
    public $default;

    /**
     * @var
     */
    protected $options;

    /**
     * @var string
     */
    protected $keyName;

    /**
     * @var array
     */
    protected $props = [];

    /**
     * 最小个数
     * @param int $num
     * @return $this
     */
    public function min(int $num): self
    {
        $this->min = $num;
        $this->rules('min:' . $num);
        return $this;
    }

    /**
     * 最大个数
     * @param int $num
     * @return $this
     */
    public function max(int $num): self
    {
        $this->max = $num;
        $this->rules('max:' . $num);
        return $this;
    }

    /**
     * 固定值配置
     * @param array $value 选中固定值
     * @param array $default 取消固定值时赋值默认值
     * @param string $text
     * @return $this
     */
    public function fixedValue(array $value = [],array $default = [],string $text)
    {
        $this->props([
            'width' => 'auto',
            'fixedValue' => $value,
            'fixedText' => $text,
            'fixedDefault' => $default,
        ]);
        return $this;
    }

    /**
     * 禁止选择
     * @param array $disableIds
     * @return SelectArea
     */
    public function disable(array $disableIds)
    {
        return $this->props('disable',$disableIds);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function jsonSerialize(): array
    {
        $areaOptions = SystemArea::cascaderOptions();
        $this->attribute('props', $this->props);
        $this->props([
            'min' => $this->min ?: 0,
            'max' => $this->max ?: 0,
            'options' => $areaOptions,
        ]);
        return array_merge(parent::jsonSerialize(), []);
    }
}