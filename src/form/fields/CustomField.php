<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 17:38:38
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:05:35
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : CustomField.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types = 1);
namespace think\admin\form\fields;

class CustomField extends Field
{
    public $component;

    /**
     * CustomField constructor.
     * @param $column
     * @param $title
     */
    public function __construct($component,$column = '',$title = '')
    {
        $this->component = $component;
        $this->column = $column;
        $this->title = $title ?: $column;
        $this->init();
    }

    /**
     * Prepare the field for JSON serialization.
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(),[]);
    }
}