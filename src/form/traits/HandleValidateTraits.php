<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 14:38:56
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 15:12:46
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : HandleValidateTraits.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types = 1);
namespace think\admin\form\traits;

use think\admin\form\fields\Field;

/**
 * Trait handleFieldsTraits
 * @package think\form\traits
 */
trait HandleValidateTraits
{
    /**
     * 获取全场景验证规则
     * @param string $type  all|update|creation
     * @param array $inputs 表单值
     * @return mixed
     */
    public function getRules($type = 'all',$inputs = [])
    {
        [$rules,$fields ]= [[],$this->getFilterFields()];
        collect($fields)->each(function(Field $field) use (&$rules,$type,$inputs){
            $fieldRules = $field->getRules($type,$inputs);
            !empty($fieldRules) && $rules[$field->getColumn()."|".$field->getTitle()] = $fieldRules;
        });
        return $rules;
    }
}