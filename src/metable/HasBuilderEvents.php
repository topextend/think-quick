<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 18:16:08
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:01:29
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : HasBuilderEvents.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\metable;

use Closure;

trait HasBuilderEvents
{
    /**
     * @var array
     */
    protected static $elementEventCallbacks = [];

    /**
     * @var string 初始化事件key
     */
    protected static $initEventKey = "init_event";

    /**
     * @param Closure|null $callback
     */
    public static function initEvent(Closure $callback = null)
    {
        static::$elementEventCallbacks[static::$initEventKey][] = $callback;
    }

    /**
     *
     */
    protected function callInitCallbacks()
    {
        if (!isset(static::$elementEventCallbacks[static::$initEventKey])
            || empty(static::$elementEventCallbacks[static::$initEventKey])) {
            return;
        }
        foreach (static::$elementEventCallbacks[static::$initEventKey] as $callback) {
            call_user_func($callback, $this);
        }
    }
}