<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 18:18:41
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:01:40
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : HasEmit.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\metable;

trait HasEmit
{
    /**
     * @var array
     */
    protected $emit = [];

    /**
     * @param $name
     * @param string $value
     * @return $this
     */
    public function emit($name, $value = '')
    {
        if (is_array($name)) {
            $this->withEmit($name);
        } else {
            $this->withEmit([$name => $value]);
        }
        return $this;
    }

    /**
     * @param array $emit
     * @return $this
     */
    protected function withEmit(array $emit)
    {
        $this->emit = array_merge($this->emit, $emit);
        return $this;
    }

    /**
     * @param string $key
     * @param string $default
     * @return array|mixed|string
     */
    protected function getEmit($key = '', $default = '')
    {
        if (empty($key)) {
            return $this->emit;
        }
        return $this->emit[$key] ?? $default;
    }
}