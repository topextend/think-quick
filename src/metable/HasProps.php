<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 18:19:24
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:01:43
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : HasProps.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\metable;

trait HasProps
{
    /**
     * @var array
     */
    protected $props = [];

    /**
     * @param $name
     * @param string $value
     * @return $this
     */
    public function props($name, $value = '')
    {
        if (is_array($name)) {
            $this->withProps($name);
        } else {
            $this->withProps([$name => $value]);
        }
        return $this;
    }

    /**
     * @param array $props
     * @return $this
     */
    protected function withProps(array $props)
    {
        $this->props = array_merge($this->props, $props);
        return $this;
    }

    /**
     * @param string $key
     * @param string $default
     * @return array|mixed|string
     */
    protected function getProps($key = '', $default = '')
    {
        if (empty($key)) {
            return $this->props;
        }
        return $this->props[$key] ?? $default;
    }
}