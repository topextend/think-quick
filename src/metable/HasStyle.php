<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 18:20:04
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:01:48
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : HasStyle.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\metable;

trait HasStyle
{
    /**
     * @var array
     */
    protected $style = [];

    /**
     * @param $name
     * @param string $value
     * @return $this
     */
    public function style($name, $value = '')
    {
        if (is_array($name)) {
            $this->withStyle($name);
        } else {
            $this->withStyle([$name => $value]);
        }
        return $this;
    }

    /**
     * @param array $style
     * @return $this
     */
    protected function withStyle(array $style)
    {
        $this->style = array_merge($this->style, $style);
        return $this;
    }

    /**
     * @param string $key
     * @param string $default
     * @return array|mixed|string
     */
    protected function getStyle($key = '', $default = '')
    {
        if (empty($key)) {
            return $this->style;
        }
        return $this->style[$key] ?? $default;
    }
}