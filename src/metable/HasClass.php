<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 18:17:21
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:01:33
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : HasClass.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\metable;

trait HasClass
{
    /**
     * @var array
     */
    protected $class = [];

    /**
     * @param $name
     * @return $this
     */
    public function setClass($name)
    {
        if (is_array($name)) {
            $this->withClass($name);
        } else {
            $this->withClass([$name => true]);
        }
        return $this;
    }

    /**
     * @param array $class
     * @return $this
     */
    protected function withClass(array $class)
    {
        $this->class = array_merge($this->class, $class);
        return $this;
    }

    /**
     * @param string $key
     * @param string $default
     * @return array|mixed|string
     */
    protected function getClass($key = '', $default = '')
    {
        if (empty($key)) {
            return $this->class;
        }
        return $this->class[$key] ?? $default;
    }
}