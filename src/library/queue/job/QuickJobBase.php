<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-26 16:24:33
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 22:04:12
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : QuickJobBase.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\library\queue\job;

use think\facade\Log;
use think\queue\Job;
use think\admin\library\queue\job\QuickJobInterface;

class QuickJobBase implements QuickJobInterface
{
    /**
     * @var Job
     */
    public $job;

    /**
     * @param Job $job
     * @return $this
     */
    public function setJob(Job $job)
    {
        $this->job = $job;
        return $this;
    }

    /**
     * @param $job
     * @return mixed|void
     */
    public function handel($job)
    {
        // TODO: Implement execute() method.
    }
}