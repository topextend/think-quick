<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-26 22:04:21
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 22:04:22
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : QuickJobInterface.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\library\queue\job;

use think\queue\Job;

/**
 * Interface QuickJobInterface
 * @package plugins\demo\jobs
 */
interface QuickJobInterface
{
    /**
     * @param $job
     * @return mixed
     */
    public function handel($job);
}