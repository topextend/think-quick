<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-26 16:23:41
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 22:03:50
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : HandleQuickJob.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\library\queue\job;

use think\admin\library\queue\job\BaseJob;

class HandleQuickJob extends BaseJob
{
    /**
     * @param $job
     * @param array $data
     * @return bool|mixed
     */
    public function handle($job, array $data)
    {
        $command = unserialize($data['command']);
        return $this->app->invoke([$command, 'handle'],[$job]);
    }

    /**
     * @param array $data
     * @param $e
     */
    public function failed(array $data,$e)
    {
        if(!empty($data['data']['command'])){
            $command = unserialize($data['data']['command']);
            if (method_exists($command, 'failed')) {
                $command->failed();
            }
        }
    }
}