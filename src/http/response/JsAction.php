<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 18:36:52
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:02:07
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : JsAction.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\http\response;

use JsonSerializable;

class JsAction implements JsonSerializable
{
    use HasAction;

    public $actions = [];

    /**
     * @return mixed
     */
    public function jsonSerialize(): array
    {
        return array_merge($this->actions);
    }
}