<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 18:41:40
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:03:10
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : EventAction.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\http\response\actions;

class EventAction extends Actions
{
    /**
     * 动作
     * @var string
     */
    public $action = 'event';

    /**
     * EmitAction constructor.
     * @param string $event
     * @param array $data
     */
    public function __construct(string $event,array $data = [] )
    {
        $this->event($event);
        $this->data($data);
    }

    /**
     * @param string $event
     * @return $this
     */
    public function event(string $event)
    {
        return $this->withParams(['event' => $event]);
    }

    /**
     * @param string $event
     * @return $this
     */
    public function isQuick()
    {
        return $this->withParams(['isQuick' => true]);
    }

    /**
     * @param array $data
     * @return eventAction
     */
    public function data(array $data)
    {
        return $this->withParams(['data' => $data]);
    }
}