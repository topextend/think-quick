<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 18:45:54
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:02:43
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : RedirectAction.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\http\response\actions;

class RedirectAction extends Actions
{
    /**
     * 动作
     * @var string
     */
    public $action = 'redirect';

    /**
     * RedirectAction constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url($url);
    }

    /**
     * 地址
     * @param string $url
     * @return $this
     */
    public function url(string $url)
    {
        $this->params = array_merge($this->params, ['url' => $url]);
        return $this;
    }
}