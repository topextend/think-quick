<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 18:44:29
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:02:53
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : PopoverAction.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\http\response\actions;

use Closure;
use think\admin\components\Component;
use think\admin\Element;

/**
 * Class PopoverAction
 * @package think\admin\http\response\actions
 */
class PopoverAction extends Actions
{
    /**
     * 动作
     * @var string
     */
    public $action = 'popover';

    /**
     * @var \think\admin\components\QuickPopover
     */
    private $popover;

    /**
     * ModalAction constructor.
     * @param Element $component
     * @param string|Closure|array $title
     */
    public function __construct($content, $title = '')
    {
        $popover = $this->getPopover();
        if ($title instanceof \Closure) {
            call_user_func($title, $popover);
        }elseif(is_array($title)){
            $popover->props($title);
        } elseif(is_string($title)){
            $popover->title($title);
        }
        $popover->content($content);
        $this->popover = $popover;
    }

    /**
     * @return \think\admin\components\QuickPopover
     */
    public function getPopover()
    {
        if (!$this->popover) {
            $popover = Component::quickPopover('title');
            $this->popover = $popover;
        }
        return $this->popover;
    }

    /**
     * @param Element|string|array $component
     * 请求链接 请求对象
     * @return PopoverAction
     */
    public function content($component)
    {
        $popover = $this->getPopover();
        $popover->content($component);
        return $this;
    }

    /**
     * @param string $title
     * @return DialogAction
     */
    public function title(string $title)
    {
        return $this->withParams([__FUNCTION__ => $title]);
    }

    public function jsonSerialize(): array
    {
        $this->withParams(['config' => $this->getPopover()]);
        return parent::jsonSerialize();
    }
}