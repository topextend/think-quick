<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-27 17:11:10
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-27 17:11:10
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : BootToolsMiddleware.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\http\middleware;

use think\admin\events\ServingQuick;
use think\admin\Quick;

class BootToolsMiddleware
{
    /**
     * @param $request
     * @param $next
     * @return mixed
     */
    public function handle($request, $next)
    {
        Quick::bootTools($request);
        return $next($request);
    }
}