<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-27 18:06:50
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-27 18:09:20
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : ResourceController.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\http\controller;

use think\admin\contracts\AuthInterface;
use think\admin\http\InteractsWithResources;
use think\admin\http\response\JsonResponse;
use think\admin\library\service\AuthService;
use think\exception\HttpException;
use think\admin\Resource;
use think\admin\Quick;

class ResourceController
{
    use InteractsWithResources;

    /**
     * @param string $resource
     * @param string $action
     * @param string $func
     * @return bool|mixed|\think\response\Json|\think\response\Redirect
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function execute($resource = '', $action = '', $func = '')
    {
        $auth = Quick::getAuthService();

        if (empty($resource) || empty($action)) {
            throw new HttpException(500, lang('resource can not be empty'));
        }

        $resourceClass = Quick::resourceForKey(app('http')->getName(), $resource);
        if (empty($resourceClass)) {
            throw new HttpException(500, lang('resource can not be empty'));
        }

        app()->request->setAction($action);
        /** @var Resource $instance */
        $instance = invoke($resourceClass);
        $vars = [];

        $instanceType = 'resource';
        if (is_callable([$instance, $action])) {
            // 执行资源类操作方法
            $call = [$instance, $action];
        } elseif (is_callable([$instance, '_empty'])) {
            // 空操作
            $call = [$instance, '_empty'];
            $vars = [$action];
        } else {
            $actionInstance = $instance->handleResourceAction($action, $func);
            if (is_callable([$actionInstance, $func]) && in_array($func, ['load', 'store', 'async'])) {
                if($func === 'store'){
                    saveDataAuth();
                }
                $call = [$actionInstance, $func];
                $instanceType = 'action';
            } else {
                throw new HttpException(404, lang('resource action %s not found', [get_class($actionInstance) . '->' . $func . '()']));
            }
        }

        $request = app()->request;
        $checkInstance = $call[0];
        $checkAction = $call[1];
        if ($instanceType === 'action') {
            $checkAction = '';
        }

        $res = $auth->checkAuth($checkInstance, $checkAction, $request);
        if ($res !== true) {
            return $res;
        }

        $res = invoke($call, $vars);
        if ($res instanceof JsonResponse) {
            return $res->send();
        }
        return $res;
    }
}