<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-27 17:03:40
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-27 17:15:22
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Route.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);

use think\admin\http\middleware\DispatchAssetsQuickEvent;
use think\admin\Quick;
use think\facade\Route;
use think\admin\http\middleware\BootToolsMiddleware;
use think\admin\http\middleware\AppModuleRunMiddleware;
use think\admin\http\middleware\DispatchServingQuickEvent;

Route::get('captcha/[:config]', '\\think\\captcha\\CaptchaController@index');

Route::group('quick', function () {
    Route::get("style/<name>", function () {
        return Quick::loadStyles();
    });
    Route::get("script/<name>", function () {
        return Quick::loadScript();
    });
    // Route::get("index", '\think\admin\http\controller\QuickController::index');
})->middleware([
    // ServeQuickMiddleware::class,
    DispatchServingQuickEvent::class,
    DispatchAssetsQuickEvent::class,
    AppModuleRunMiddleware::class,
]);

Route::group('resource', function () {
    Route::rule('<resource>/<action>/<func?>', '\think\admin\http\controller\ResourceController::execute');
})->middleware(array_merge([
    // AdminAuthMiddleware::class,
    AppModuleRunMiddleware::class,
    BootToolsMiddleware::class,
    DispatchServingQuickEvent::class
],config('quick.middleware',[])));