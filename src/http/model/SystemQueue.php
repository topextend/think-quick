<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-26 16:21:36
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-27 17:59:09
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : SystemQueue.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types = 1);
namespace think\admin\http\model;

use think\admin\http\model\Model;

/**
 * Class SystemQueue
 * @package think\admin\http\model
 */
class SystemQueue extends Model
{
    protected $name = 'system_queue';
}