<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 20:36:03
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:08:27
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Element.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin;

use JsonSerializable;
use think\admin\metable\HasBuilderEvents;
use think\admin\metable\HasClass;
use think\admin\metable\HasDomProps;
use think\admin\metable\HasEmit;
use think\admin\metable\HasProps;
use think\admin\metable\HasStyle;
use think\admin\metable\Metable;

abstract class Element implements JsonSerializable
{
    use Metable,
        HasDomProps,
        HasClass,
        HasStyle,
        HasEmit,
        HasBuilderEvents,
        HasProps,
        AuthorizedToSee;

    /**
     *  组件名称.
     * @var string
     */
    public $component;

    /**
     * 自定义组件
     * @var array
     */
    protected static $customComponents = [];

    /**
     * Element constructor.
     * @param null $component
     */
    public function __construct($component = null)
    {
        $this->component = $component ?? $this->component;
        $this->callInitCallbacks();
    }

    /**
     * @param mixed ...$arguments
     * @return static
     */
    public static function make(...$arguments)
    {
        return new static(...$arguments);
    }

    /**
     * 获取字段使用的组件名称
     * @return string
     */
    public function component()
    {
        if (isset(static::$customComponents[get_class($this)])) {
            return static::$customComponents[get_class($this)];
        }
        return $this->component;
    }

    /**
     * 设置字段使用的前端组件
     * @param $component
     */
    public static function useComponent($component)
    {
        static::$customComponents[get_called_class()] = $component;
    }

    /**
     * 获取当前组件的的子类
     * @return array
     */
    public function getChildrenComponents(): array
    {
        if (is_array($this->children)) {
            return $this->children;
        }
        return [$this->children];
    }

    /**
     * @param array $data
     * @return array
     */
    protected function getExtraAttrs(array $data = []): array
    {
        if ($slot = $this->getSlot()) {
            $data['slot'] = $slot;
        }
        if ($class = $this->getClass()) {
            $data['class'] = $class;
        }
        if ($style = $this->getStyle()) {
            $data['style'] = $style;
        }
        if ($domProps = $this->getDomProps()) {
            $data['domProps'] = $domProps;
        }

        return $data;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize(): array
    {
        $props = array_merge($this->getProps(), $this->getAttributes(), $this->getExtraAttrs());
        return array_merge([
            'component' => $this->component(),
            'children' => $this->getChildren(),
            'props' => $props,// component 组件的props
        ], $this->meta());
    }
}