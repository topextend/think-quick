<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-26 16:45:08
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 16:45:08
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Common.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
use think\admin\library\service\SystemService;
if (!function_exists('strAfter')) {
    /**
     * 返回给定值第一次出现后字符串的剩余部分
     * @param string $subject
     * @param string $search
     * @return string
     */
    function strAfter($subject, $search)
    {
        return $search === '' ? $subject : array_reverse(explode($search, $subject, 2))[0];
    }
}
if (!function_exists('renderPluginsComponents')) {
    /**
     * @throws Exception
     */
    function renderPluginsComponents()
    {
        \think\admin\library\service\PluginService::instance()->readerPluginComponent();
    }
}
if (!function_exists('readerPluginComponentByName')) {
    /**
     * @param string $name
     * @throws Exception
     */
    function readerPluginComponentByName(string $name)
    {
        \think\admin\library\service\PluginService::instance()->readerPluginComponentByName($name);
    }
}
if (!function_exists('availableScripts')) {
    function availableScripts()
    {
        return \think\admin\quick::availableScripts(app("request"));
    }
}
if (!function_exists('jsonVariables')) {
    function jsonVariables()
    {
        return json_encode(\think\admin\quick::jsonVariables(app("request")));
    }
}
if (!function_exists('availableStyles')) {
    function availableStyles()
    {
        return \think\admin\quick::availableStyles(app("request"));
    }
}

if (!function_exists('sysConfig')) {
    /**
     * 获取系统参数
     *
     * @param string $name
     * @param null $value
     * @param string $plugin
     * @return array|int|mixed|string
     */
    function sysConfig($name = '', $value = null,string $plugin = 'admin')
    {
        try {
            return SystemService::instance()->get($name,$value,$plugin);
        }catch (\Exception $e){
            return '';
        }
    }
}