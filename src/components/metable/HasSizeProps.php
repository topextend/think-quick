<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 16:46:58
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:06:11
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : HasSizeProps.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\components\metable;

trait HasSizeProps
{
    /**
     * @param string $size medium / small / mini
     * @return $this
     */
    public function size(string $size)
    {
        $this->attribute("size",$size);
        return $this;
    }
}