<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 17:25:39
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:06:26
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : QuickDialog.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\components\element;

use think\admin\Element;
use think\admin\form\Form;

class QuickDialog extends Element
{
    public $component = "quick-dialog";

    /**
     * CustomField constructor.
     * @param $column
     * @param $title
     */
    public function __construct($title = '')
    {
        $title && $this->title($title);
        $this->props('type' ,'dialog');
        $this->attribute([
            "lock-scroll" => false,
            "top" => '10vh'
        ])->maxHeight("65vh")->width('750px');
    }

    /**
     * @param string $title
     * @return QuickDialog
     */
    public function title(string $title)
    {
        $this->withMeta(["title" => $title]);
        return $this->props(["title" => $title]);
    }

    /**
     * Prepare the field for JSON serialization.
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), []);
    }

    /**
     * @param string $width
     * @return $this
     */
    public function width(string $width)
    {
        is_numeric($width) && $width = $width . "px";
        $this->props("width", $width);
        return $this;
    }

    /**
     * @param $component
     * @param string $slot
     * @return Element
     */
    public function children($component, $slot = '')
    {
        return parent::children($component, $slot);
    }

    /**
     * @param string $height
     * @return $this
     */
    public function height(string $height)
    {
        $this->props("height", $height);
        return $this;
    }

    /**
     * @param string $height
     * @return $this
     */
    public function maxHeight(string $height)
    {
        $this->props("max-height", $height);
        return $this;
    }
}