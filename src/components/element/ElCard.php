<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 16:44:54
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:06:52
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : ElCard.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\components\element;

use think\admin\Element;

class ElCard extends Element
{
    public $component = "el-card";

    /**
     * ElCard constructor.
     * @param Element|null $content
     */
    public function __construct($content = null)
    {
        $content && $this->children($content);
    }

    /**
     * 设置头部内容
     * @param Element $header 头部内容
     * @return $this
     */
    public function header(Element $header)
    {
        $this->children($header, "header");
        return $this;
    }
}
