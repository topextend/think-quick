<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 16:51:02
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:06:47
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : ElDrawer.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\components\element;

use think\admin\Element;
use think\admin\form\Form;

class ElDrawer extends Element
{
    public $component = "el-drawer";

    /**
     * ElDrawer constructor.
     * @param string $title
     */
    public function __construct($title = '')
    {
        $title && $this->withMeta(["title" => $title]);
        $this->props('type' ,'drawer');
        $this->props([
            "append-to-body" => false
        ])->size('50%');
    }
    
    /**
     * @param string $title
     * @return Dialog
     */
    public function title(string $title)
    {
        $this->withMeta(["title" => $title]);
        return $this->props(["title" => $title]);
    }

    /**
     * Prepare the field for JSON serialization.
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), []);
    }

    /**
     * @param string $size
     * @return $this
     */
    public function size(string $size)
    {
        $this->props("size", $size);
        return $this;
    }

    /**
     * Drawer 打开的方向
     * @param string $direction rtl / ltr / ttb / btt
     * @return $this
     */
    public function direction(string $direction)
    {
        $this->props("direction", $direction);
        return $this;
    }

    /**
     * @param $component
     * @param string $slot
     * @return Element
     */
    public function children($component, $slot = '')
    {
        return parent::children($component, $slot);
    }
}