<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 17:04:28
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:06:39
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : ElLink.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\components\element;

use think\admin\components\metable\HasSizeProps;
use think\admin\Element;

class ElLink extends Element
{
    public $component = "el-link";

    /**
     * ElLink constructor.
     * @param string $type
     */
    public function __construct(string $type = '')
    {
        $type && $this->type($type);
    }

    /**
     * @param string $type primary / success / warning / danger / info
     * @return $this
     */
    public function type(string $type)
    {
        $this->attribute("type", $type);
        return $this;
    }

    /**
     * @param string $icon
     * @return $this
     */
    public function icon(string $icon)
    {
        $this->attribute("icon", $icon);
        return $this;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function href(string $url)
    {
        $this->attribute(__FUNCTION__, $url);
        return $this;
    }

    /**
     * @return $this
     */
    public function disabled()
    {
        $this->attribute(__FUNCTION__, true);
        return $this;
    }

    /**
     * @return $this
     */
    public function underline()
    {
        $this->attribute(__FUNCTION__, false);
        return $this;
    }
}