<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 16:40:39
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 15:14:26
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Component.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\components;

use think\admin\components\element\ElButton;
use think\admin\components\element\ElCard;
use think\admin\components\element\ElDescriptions;
use think\admin\components\element\ElDrawer;
use think\admin\components\element\ElImage;
use think\admin\components\element\ElLink;
use think\admin\components\element\ElPopover;
use think\admin\components\element\ElTabsPane;
use think\admin\components\element\ElTooltip;
use think\admin\components\layout\Content;
use think\admin\components\element\QuickIcon;
use think\admin\components\element\Confirm;
use think\admin\components\element\ElInput;
use think\admin\components\element\ElTable;
use think\admin\components\element\QuickDialog;
use think\admin\components\layout\Row;
use think\admin\Element;
use think\admin\http\response\actions\Actions;
use think\Exception;

/**
 * Class Component
 * @package think\components
 * @method static Custom           custom(string $componentName) 自定义组件
 * @method static ElInput          input(string $name, string $title)
 * @method static ElTable          table(string $name, string $title)
 * @method static QuickDialog      dialog(string $title)
 * @method static Confirm          confirm($msg, $confirm, $cancel, $title)
 * @method static QuickIcon        icon(string $icon, string $size = '',string $color = '')
 * @method static ElPopover        popover(string $content)
 * @method static ElTooltip        tooltip(string $msg,$content = '',string $placement = '')
 * @method static ElButton         button(string $content = '', string $type = '', string $size = '')
 * @method static ElLink           link(string $type)
 * @method static ElImage          image(string $src,int $width,int $height)
 * @method static Content          content()
 * @method static ShowHtml         html(string $html)
 * @method static Iframe           iframe(string $src)
 * @method static ElDrawer         drawer(string $title)
 * @method static QuickTabs        tabs(array $panes = [], string $type = '')
 * @method static ElTabsPane       tabsPane(string $name, $label, $content)
 * @method static Row              row(string $type)
 * @method static QuickAction      action($name, string $type = '', string $size = '')
 * @method static ElCard           card($content = null)
 * @method static QuickPopover     quickPopover($content = null)
 * @method static InlineEdit       inline($content = null)
 * @method static QuickTree        tree(array $data = [],string $title = '')
 * @method static QkImage          qkImage(string $src,int $width,int $height)
 * @method static ElDescriptions   elDescriptions($title = '',$extra = null)
 * @method static QkDescriptions   qkDescriptions($title = '',$extra = null)
 * @method static IconCard         iconCard(string $title, string $number, string $icon = '')
 * @method static SwitchContent    switchContent($active,$inactive,string $key,$condition,$value)
 */
class Component
{
    /**
     * @var array
     */
    public static $availableComponents = [
        'custom' => Custom::class,
        'input' => ElInput::class,
        'table' => ElTable::class,
        'dialog' => QuickDialog::class,
        'confirm' => Confirm::class,
        'icon' => QuickIcon::class,
        'popover' => ElPopover::class,
        'quickPopover' => QuickPopover::class,
        'tooltip' => ElTooltip::class,
        'button' => ElButton::class,
        'link' => ElLink::class,
        'content' => Content::class,
        'html' => ShowHtml::class,
        'iframe' => Iframe::class,
        'drawer' => ElDrawer::class,
        'tabs' => QuickTabs::class,
        'tabsPane' => ElTabsPane::class,
        'row' => Row::class,
        'action' => QuickAction::class,
        'card' => ElCard::class,
        'image' => ElImage::class,
        'inline' => InlineEdit::class,
        'tree' => QuickTree::class,
        'qkImage' => QkImage::class,
        'elDescriptions' => ElDescriptions::class,
        'qkDescriptions' => QkDescriptions::class,
        'iconCard' => IconCard::class,
        'switchContent' => SwitchContent::class,
    ];

    public static function cardMenu($icon, $name, $color)
    {
        return self::card(self::custom("a")->content([
            self::icon($icon, '30')->color($color),
            self::custom("p")->content($name)
        ]))->style(["text-align" => "center", 'cursor' => 'pointer', 'margin-bottom' => "15px"])
            ->props('shadow', "hover");
    }

    /**
     * 扩展组件
     * @param string $name
     * @param $component
     */
    public static function extend(string $name, $component)
    {
        static::$availableComponents[$name] = $component;
    }

    /**
     * @param $name
     * @return bool|mixed|string
     */
    public static function findComponentClass($name)
    {
        $class = static::$availableComponents[$name] ?? '';
        if (!empty($class) && class_exists($class)) {
            return $class;
        }
        return false;
    }

    /**
     * @param $name
     * @param mixed ...$arguments
     * @return Element
     * @throws Exception
     */
    public static function create($name, ...$arguments)
    {
        if ($className = static::findComponentClass($name)) {
            /** @var Element $object */
            $object = new $className(...$arguments);
            return $object;
        }
        throw new Exception('找不到组件');
    }

    /**
     * @param $method
     * @param $arguments
     * @return Element
     * @throws Exception
     */
    public function __call($method, $arguments)
    {
        return $this->create($method, ...$arguments);
    }

    /**
     * @param $method
     * @param $arguments
     * @return Element
     * @throws Exception
     */
    public static function __callStatic($method, $arguments)
    {
        return static::create($method, ...$arguments);
    }
}