<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-24 14:37:10
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:05:58
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : QuickTabs.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\components;

use think\admin\components\element\ElTabs;

class QuickTabs extends ElTabs
{
    public $component = "quick-tabs";

    /**
     *  去掉底部margin-bottom
     * @return $this
     */
    public function removeBottom()
    {
        $this->props('remove-bottom',true);
        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function default(string $value)
    {
        $this->props('default',$value);
        return $this;
    }

    /**
     * @return $this
     */
    public function isFilter()
    {
        $this->props('isFilter',true);
        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function tabKey(string $value)
    {
        $this->props('tabKey',$value);
        return $this;
    }
}