<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 17:12:10
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 13:06:19
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Content.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin\components\layout;

use think\admin\components\Component;
use think\admin\Element;

/**
 * Class Content
 */
class Content extends Element
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var
     */
    public $description;

    /**
     * @var string
     */
    public $component = "quick-page";

    /**
     * 设置标题
     * @param string $title
     * @return $this
     */
    public function title(string $title)
    {
        $this->title = $title;
        return $this;
    }

    public function description(string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param $body
     * @return $this
     */
    public function body($body)
    {
        return $this->row($body);
    }

    /**
     * @param $content
     * @return $this
     */
    public function row($content)
    {
        if ($content instanceof \Closure) {
            $row = Row::make();
            call_user_func($content, $row);
        } else {
            $row = Row::make($content);
        }
        $this->children($row);
        return $this;
    }

    /**
     * 隐藏header
     * @return Content
     */
    public function hideHeader()
    {
        return $this->attribute(['showHeader' => false]);
    }

    /**
     * @return $this
     */
    public function cleanBackground()
    {
        $this->props('pageStyle', [
            'padding' => 0,
            'backgroundColor' => 'transparent',
        ]);
        return $this;
    }

    public function jsonSerialize(): array
    {
        $this->props("title", $this->title);
        $this->props("description", $this->description);
        return array_merge(parent::jsonSerialize(), []);
    }
}