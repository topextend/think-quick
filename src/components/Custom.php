<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 17:24:12
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 15:14:15
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Custom.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types = 1);
namespace think\admin\components;

use think\admin\Element;

/**
 * 自定义组件
 * Class Custom
 * @package think\components
 */
class Custom extends Element
{
    public $component;

    /**
     * Custom constructor.
     * @param $component
     */
    public function __construct($component)
    {
        $this->component = $component;
    }
    /**
     * Prepare the field for JSON serialization.
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(),[]);
    }
}