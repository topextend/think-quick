<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-23 16:13:18
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-24 14:45:17
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Controller.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\admin;

use stdClass;
use think\App;
use think\Request;

/**
 * 标准控制器基类
 * Class Controller
 * @package think\admin
 */
class Controller extends stdClass
{
    /**
     * 应用容器
     * @var App
     */
    public $app;

    /**
     * 请求对象
     * @var Request
     */
    public $request;

    /**
     * 构造方法
     * @access public
     * @param App $app 应用对象
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->request = $this->app->request;

        // 控制器初始化
        $this->initialize();
    }

    /**
     * 控制器初始化
     */
    protected function initialize()
    {
        //
    }

    /**
     * 返回JSON数据
     * @param $data
     * @return \think\response\Json
     */
    protected function responseJson($data)
    {
        if(!isset($data['code'])){
            $data = [
                'code' => 0,
                'msg' => '',
                'data' => $data,
            ];
        }
        return json($data);
    }
}